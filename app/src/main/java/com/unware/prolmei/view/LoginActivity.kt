package com.unware.prolmei.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.FirebaseApp
import kotlinx.android.synthetic.main.activity_login.*
import com.google.firebase.auth.FirebaseAuth
import com.unware.prolmei.R

class LoginActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private var mAuthListener: FirebaseAuth.AuthStateListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        FirebaseApp.initializeApp(this)

        mAuth = FirebaseAuth.getInstance()
        mAuthListener = FirebaseAuth.AuthStateListener { firebaseAuth ->
            val user = firebaseAuth.currentUser

            if (user != null) {
                goToMenu()
            }
        }

        email_sign_in_button.setOnClickListener {
            authenticate(email.text.toString(), password.text.toString())
        }

        button_nativate_register.setOnClickListener {
            goToRegister()
        }
    }

    override fun onStart() {
        super.onStart()

        mAuth!!.addAuthStateListener(mAuthListener!!)
    }

    override fun onStop() {
        super.onStop()
        mAuth!!.removeAuthStateListener(mAuthListener!!)
    }

    private fun authenticate(email: String, password: String) {
        mAuth!!.signInWithEmailAndPassword(email, password).addOnCompleteListener(
            this
        ) { task ->
            if (task.isSuccessful) {
                Log.d("AUTH", "Login Efetuado com sucesso!!!")
                goToMenu()
            } else {
                Log.w("AUTH", "Falha ao efetuar o Login: ", task.exception)
            }
        }
    }

    private fun goToRegister() {
        val intent = Intent(this, RegisterActivity::class.java)

        startActivity(intent)
    }

    private fun goToMenu() {
        val intent = Intent(this, MenuActivity::class.java)

        startActivity(intent)
    }
}