package com.unware.prolmei.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

import com.unware.prolmei.R
import com.unware.prolmei.model.Product
import kotlinx.android.synthetic.main.fragment_merchant.name
import kotlinx.android.synthetic.main.fragment_product.*


class ProductFragment : Fragment() {

    private lateinit var database: DatabaseReference
    private lateinit var productReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
        val userKey = FirebaseAuth.getInstance().currentUser!!.uid
        productReference = database.child("Merchant").orderByChild("userKey").equalTo(userKey).ref.child("Products")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_product, container, false)
        val createButton = view.findViewById<Button>(R.id.create_button)

        createButton.setOnClickListener {
            val product = Product(
                name.text.toString(),
                description.text.toString(),
                price.text.toString())

            createProduct(product)
        }

        return view
    }

    private fun createProduct(product: Product) {
        val key = productReference.push().key

        productReference.child(key!!).setValue(product)
    }
}
