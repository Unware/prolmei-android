package com.unware.prolmei.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.unware.prolmei.R
import com.unware.prolmei.model.Merchant
import kotlinx.android.synthetic.main.fragment_merchant.*

class MerchantFragment : Fragment() {
    private lateinit var database: DatabaseReference
    private lateinit var merchantReference: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        database = FirebaseDatabase.getInstance().reference
        merchantReference = database.child("Merchant")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_merchant, container, false)
        val createButton = view.findViewById<Button>(R.id.create_button)
        val userKey = FirebaseAuth.getInstance().uid.toString()

        createButton.setOnClickListener {
            val merchant = Merchant(
                name.text.toString(),
                cep.text.toString(),
                cnpj_cpf.text.toString(),
                userKey)

            createMerchant(merchant)
        }

        return view
    }

    private fun createMerchant(merchant: Merchant) {
        val key = merchantReference.push().key

        merchantReference.child(key!!).setValue(merchant)
    }
}
